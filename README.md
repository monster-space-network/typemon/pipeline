# Pipeline - [![version](https://img.shields.io/npm/v/@typemon/pipeline.svg)](https://www.npmjs.com/package/@typemon/pipeline) [![license](https://img.shields.io/npm/l/@typemon/pipeline.svg)](https://gitlab.com/monster-space-network/typemon/pipeline/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/pipeline/dev/typescript.svg) [![gitlab-pipeline](https://gitlab.com/monster-space-network/typemon/pipeline/badges/master/pipeline.svg)](https://gitlab.com/monster-space-network/typemon/pipeline/-/pipelines) [![coverage](https://gitlab.com/monster-space-network/typemon/pipeline/badges/master/coverage.svg)](https://gitlab.com/monster-space-network/typemon/pipeline/-/graphs/master/charts)



## Installation
```
$ npm install @typemon/pipeline reflect-metadata
```

### [TypeScript](https://github.com/Microsoft/TypeScript)
Configure metadata and decorator options.
```json
{
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true
}
```

### [Reflect Metadata](https://github.com/rbuckton/reflect-metadata)
Import only once at the application entry point.
```typescript
import 'reflect-metadata';
```



## Usage
Sorry. Not yet documented.
Please refer to the test code.
