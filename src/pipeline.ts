import { Check } from '@typemon/check';
import { Metadata } from '@typemon/metadata';
import { Container } from '@typemon/dependency-injection';

import { MetadataKey } from './metadata-key';
import { Pipeable } from './pipeable';
import { Resolver } from './resolver';

export function Pipeline(...pipes: ReadonlyArray<Pipeable>): PropertyDecorator & MethodDecorator & ParameterDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        if (metadata.hasOwn(MetadataKey.PIPES)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        metadata.set(MetadataKey.PIPES, pipes);

        if (Check.isUndefined(metadata.owner.propertyKey) || Check.isFunction(metadata.owner.target)) {
            return;
        }

        const constructor: Metadata = Metadata.of({
            target: metadata.owner.target.constructor,
        });
        const propertyKeys: Set<string | symbol> = constructor.hasOwn(MetadataKey.PROPERTY_KEYS)
            ? constructor.getOwn(MetadataKey.PROPERTY_KEYS)
            : new Set();

        constructor.set(MetadataKey.PROPERTY_KEYS, propertyKeys.add(metadata.owner.propertyKey));
    });
}
export namespace Pipeline {
    export function of(metadata: Metadata): ReadonlyArray<Pipeable> {
        if (metadata.hasNotOwn(MetadataKey.PIPES)) {
            return [];
        }

        return metadata.getOwn(MetadataKey.PIPES);
    }

    export interface ResolutionContext {
        readonly value: unknown;
        readonly pipes: ReadonlyArray<Pipeable>;
        readonly container?: Container;
    }

    export async function resolve(context: ResolutionContext): Promise<any> {
        return Resolver.resolve(context.value, context.pipes, context.container);
    }

    export function Model(): ClassDecorator {
        return Metadata.decorator((metadata: Metadata): void => {
            if (metadata.hasOwn(MetadataKey.MODEL)) {
                throw new Error('This decorator cannot be used in duplicate.');
            }

            const parentPropertyKeys: ReadonlySet<string | symbol> = Check.isNotNull(metadata.parent)
                ? metadata.parent.hasOwn(MetadataKey.PROPERTY_KEYS)
                    ? metadata.parent.getOwn(MetadataKey.PROPERTY_KEYS)
                    : new Set()
                : new Set();
            const propertyKeys: Set<string | symbol> = metadata.hasOwn(MetadataKey.PROPERTY_KEYS)
                ? metadata.getOwn(MetadataKey.PROPERTY_KEYS)
                : new Set();

            for (const propertyKey of parentPropertyKeys) {
                propertyKeys.add(propertyKey);
            }

            metadata.set(MetadataKey.PROPERTY_KEYS, propertyKeys);
            metadata.set(MetadataKey.MODEL, null);
        });
    }
}
