import { Constructor } from '@typemon/dependency-injection';

import { Pipe } from './pipe';

export type Pipeable = Constructor | Pipe.Constructor | Pipe.Simple;
