export namespace MetadataKey {
    export const OPTIONS: string = 'typemon.pipeline.options';

    export const PIPES: string = 'typemon.pipeline.pipes';

    export const MODEL: string = 'typemon.pipeline.model';
    export const PROPERTY_KEYS: string = 'typemon.pipeline.property-keys';
}
