import { Metadata } from '@typemon/metadata';
import { Container, Injectable } from '@typemon/dependency-injection';

import { MetadataKey } from './metadata-key';

export interface Pipe {
    apply(value: any): unknown;
}
export function Pipe(options: Pipe.Options = {}): ClassDecorator {
    return Metadata.decorator((metadata: Metadata): void => {
        if (metadata.hasOwn(MetadataKey.OPTIONS)) {
            throw new Error('This decorator cannot be used in duplicate.');
        }

        metadata.set(MetadataKey.OPTIONS, options);
        metadata.decorate([
            Injectable(),
        ]);
    });
}
export namespace Pipe {
    export type Constructor = new (...parameters: ReadonlyArray<any>) => Pipe;

    export interface Options {
        /**
         * @default false
         */
        readonly passthrough?: boolean;
    }

    export type Simple = (value: any, container: Container) => unknown;
}
