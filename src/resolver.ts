import { Check } from '@typemon/check';
import { Metadata } from '@typemon/metadata';
import { Constructor, Container } from '@typemon/dependency-injection';

import { MetadataKey } from './metadata-key';
import { Pipe } from './pipe';
import { Pipeable } from './pipeable';

export namespace Resolver {
    async function model(value: any, target: Constructor, container: Container): Promise<unknown> {
        const result: any = new target();
        const propertyKeys: ReadonlySet<string | symbol> = Metadata.getOwn({
            key: MetadataKey.PROPERTY_KEYS,
            target,
        });

        for (const propertyKey of propertyKeys) {
            const pipes: ReadonlyArray<Pipeable> = Metadata.get({
                key: MetadataKey.PIPES,
                target: target.prototype,
                propertyKey,
            });
            const input: unknown = value[propertyKey];
            const output: unknown = await resolve(input, pipes, container);

            result[propertyKey] = output;
        }

        return result;
    }

    async function apply(value: unknown, target: Pipe.Constructor, container: Container): Promise<unknown> {
        const options: Pipe.Options = Metadata.getOwn({
            key: MetadataKey.OPTIONS,
            target,
        });
        const pipe: Pipe = container.isBound(target)
            ? await container.get(target)
            : await container.instantiate(target);
        const result: unknown = await pipe.apply(value);

        if (Check.isTrue(options.passthrough)) {
            return value;
        }

        return result;
    }

    export async function resolve(input: unknown, pipeables: ReadonlyArray<Pipeable>, container: Container = new Container()): Promise<unknown> {
        const pipes: ReadonlyArray<Pipe.Simple> = pipeables.map((pipe: any): Pipe.Simple => {
            const metadata: Metadata = Metadata.of({
                target: pipe,
            });

            if (metadata.hasOwn(MetadataKey.MODEL)) {
                return (value: unknown): Promise<unknown> => model(value, pipe, container);
            }

            if (metadata.hasOwn(MetadataKey.OPTIONS)) {
                return (value: unknown): Promise<unknown> => apply(value, pipe, container);
            }

            return pipe;
        });

        if (Check.equal(pipes.length, 0)) {
            return input;
        }

        let output: unknown = input;

        for (const pipe of pipes) {
            output = await pipe(output, container);
        }

        return output;
    }
}
