import { Check } from '@typemon/check';
import { Container } from '@typemon/dependency-injection';

import { Pipe } from '../pipe';
import { Pipeable } from '../pipeable';
import { Pipeline } from '../pipeline';

export function iterate(selector: (item: any) => Pipeable | ReadonlyArray<Pipeable>): Pipe.Simple {
    return async (input: Iterable<unknown> | AsyncIterable<unknown>, container: Container): Promise<unknown> => {
        const output: Array<unknown> = [];

        if (Check.isString(input) || Check.isIterable(input)) {
            for (const item of input) {
                const pipeables: Pipeable | ReadonlyArray<Pipeable> = selector(item);
                const pipes: ReadonlyArray<Pipeable> = Check.isArray(pipeables)
                    ? pipeables
                    : [pipeables];
                const result: unknown = await Pipeline.resolve({
                    value: item,
                    pipes,
                    container,
                });

                output.push(result);
            }
        }
        else {
            for await (const item of input) {
                const pipeables: Pipeable | ReadonlyArray<Pipeable> = selector(item);
                const pipes: ReadonlyArray<Pipeable> = Check.isArray(pipeables)
                    ? pipeables
                    : [pipeables];
                const result: unknown = await Pipeline.resolve({
                    value: item,
                    pipes,
                    container,
                });

                output.push(result);
            }
        }

        return output;
    };
}
