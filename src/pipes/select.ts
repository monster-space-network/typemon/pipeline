import { Check } from '@typemon/check';
import { Container } from '@typemon/dependency-injection';

import { Pipe } from '../pipe';
import { Pipeable } from '../pipeable';
import { Pipeline } from '../pipeline';

export function select(selector: (value: any) => Pipeable | ReadonlyArray<Pipeable>): Pipe.Simple {
    return async (value: unknown, container: Container): Promise<unknown> => {
        const pipeables: Pipeable | ReadonlyArray<Pipeable> = selector(value);
        const pipes: ReadonlyArray<Pipeable> = Check.isArray(pipeables)
            ? pipeables
            : [pipeables];
        const result: unknown = await Pipeline.resolve({
            value,
            pipes,
            container,
        });

        return result;
    };
}
