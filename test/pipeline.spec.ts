import { Metadata } from '@typemon/metadata';

import { Pipeline, Pipe, MetadataKey } from '../src';

test('duplicate', (): void => {
    @Pipe()
    class FooPipe implements Pipe {
        public apply(): void { }
    }

    @Pipe()
    class BarPipe implements Pipe {
        public apply(): void { }
    }

    expect((): void => {
        class Target {
            public constructor(
                @Pipeline(FooPipe)
                @Pipeline(BarPipe)
                _: unknown
            ) { }
        }
    }).toThrow('This decorator cannot be used in duplicate.');
});

describe('model', (): void => {
    test('duplicate', (): void => {
        expect((): void => {
            @Pipeline.Model()
            @Pipeline.Model()
            class Target { }
        }).toThrow('This decorator cannot be used in duplicate.');
    });


    test('modeling', (): void => {
        class Empty { }

        @Pipeline.Model()
        class Parent extends Empty {
            @Pipeline(
                (): void => { },
            )
            public readonly foo: unknown;
        }

        @Pipeline.Model()
        class Child extends Parent {
            @Pipeline(
                (): void => { },
            )
            public static readonly never: unknown;

            @Pipeline(
                (): void => { },
            )
            public readonly bar: unknown;

            @Pipeline(
                (): void => { },
            )
            public baz(): void { }
        }

        expect(Array.from(Metadata.getOwn({ key: MetadataKey.PROPERTY_KEYS, target: Parent }))).toEqual(['foo']);
        expect(Array.from(Metadata.getOwn({ key: MetadataKey.PROPERTY_KEYS, target: Child }))).toEqual(['bar', 'baz', 'foo']);
    });
});

test('resolve', async (): Promise<void> => {
    class Target {
        public constructor(
            @Pipeline(
                (value: number): string => value.toString(),
            )
            _: unknown,
        ) { }
    }

    expect(Pipeline.resolve({
        value: 1,
        pipes: Pipeline.of(Metadata.of({
            target: Target,
            parameterIndex: 0,
        })),
    })).resolves.toBe('1');

    expect(Pipeline.resolve({
        value: 1,
        pipes: Pipeline.of(Metadata.of({
            target: Target,
            parameterIndex: 1,
        })),
    })).resolves.toBe(1);
});
