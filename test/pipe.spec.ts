import { Pipe } from '../src';

test('duplicate', () => {
    expect(() => {
        @Pipe()
        @Pipe()
        class ExamplePipe implements Pipe {
            public apply(): void { }
        }
    }).toThrow('This decorator cannot be used in duplicate.');
});
