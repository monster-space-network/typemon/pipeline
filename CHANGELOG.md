# [2.0.0-next.2](https://gitlab.com/monster-space-network/typemon/pipeline/compare/2.0.0-next.1...2.0.0-next.2) (2021-01-31)


### Features

* **pipe:** 수명 옵션 삭제 ([4bc2bf3](https://gitlab.com/monster-space-network/typemon/pipeline/commit/4bc2bf329837d08a9e20d3505dd960824bc0b198))



# [2.0.0-next.1](https://gitlab.com/monster-space-network/typemon/pipeline/compare/2.0.0-next.0...2.0.0-next.1) (2021-01-28)


### Features

* **pipeline:** 메서드 데코레이터 지원 추가 ([38bb79c](https://gitlab.com/monster-space-network/typemon/pipeline/commit/38bb79c3ae533b7aeb8e3885f7b66cff80d4e362))
* 파이프라인 사용 방식 변경 ([11dfb6e](https://gitlab.com/monster-space-network/typemon/pipeline/commit/11dfb6ef44412757cb408ce86443565a3a4217f2))



# [2.0.0-next.0](https://gitlab.com/monster-space-network/typemon/pipeline/compare/2.0.0-beta.2...2.0.0-next.0) (2021-01-27)


### Features

* **pipe:** 간단한 파이프 기능 변경 ([4de566f](https://gitlab.com/monster-space-network/typemon/pipeline/commit/4de566fa9b479f424fc3809cbef201ee33aa1ab8))
* **pipe:** 반환 유형 변경 ([55c52a3](https://gitlab.com/monster-space-network/typemon/pipeline/commit/55c52a3c94efac75cfce219750d8dd57616e07ff))
* **pipeline:** 검증 기능 삭제 ([8eed37c](https://gitlab.com/monster-space-network/typemon/pipeline/commit/8eed37c84b1fb4a85f2989701c1fafbdbe7969cd))
* **pipeline:** resolver 적용 및 해결 방식 변경 ([d3c2972](https://gitlab.com/monster-space-network/typemon/pipeline/commit/d3c297209cbfd113301574e0a90c6f7e8c3e2a5b))
* **pipes:** select, iterate 추가 ([922c51f](https://gitlab.com/monster-space-network/typemon/pipeline/commit/922c51f5216b5743df7734242002c5667f3c5453))
* **resolver:** 조기 반환 로직 추가 ([9d73811](https://gitlab.com/monster-space-network/typemon/pipeline/commit/9d738117accc8b2834e01a6ed3680881ea478e6a))
* **resolver.model:** 인스턴스화 기능 추가 ([f66f1d8](https://gitlab.com/monster-space-network/typemon/pipeline/commit/f66f1d8e228edbfbf77612b14bd72b6facc90a5f))
* @typemon/metadata 의존성 업데이트 ([85ff478](https://gitlab.com/monster-space-network/typemon/pipeline/commit/85ff478b7def7f8256052b777dc1a265ac6f5731))
* @typemon/metadata 의존성 추가 ([266cdb9](https://gitlab.com/monster-space-network/typemon/pipeline/commit/266cdb9de8d57e6b46f257b0c5cbccb3f7946abc))
* @typemon/reflection 의존성 삭제 ([bc367e3](https://gitlab.com/monster-space-network/typemon/pipeline/commit/bc367e3c64e852880b905f3d0ec29d5a3ab7e460))
* 간단한 파이프 기능 추가 ([9ad5d14](https://gitlab.com/monster-space-network/typemon/pipeline/commit/9ad5d145b4743c60e334c1acaee7eab8852df84e))
* 메타데이터 기능 삭제 ([74b9d13](https://gitlab.com/monster-space-network/typemon/pipeline/commit/74b9d132ba94d43e34bae85473d95e2215cd1935))
* 모델 기능 추가 ([bc92988](https://gitlab.com/monster-space-network/typemon/pipeline/commit/bc92988a2ab47766f06754ff373be04a45e27c53))
* 의존성 변경 사항 반영 ([2372b3f](https://gitlab.com/monster-space-network/typemon/pipeline/commit/2372b3f42c1df9c55ef0b23f9640d1b675c4cf92))
* 의존성 업데이트 ([6542699](https://gitlab.com/monster-space-network/typemon/pipeline/commit/6542699608ba956d4876383f6df12ee1bfcccd88))
* pipeable 삭제 ([1c49183](https://gitlab.com/monster-space-network/typemon/pipeline/commit/1c49183cb522560a858573388a99484418d0223b))
* pipeable 추가 ([bb92377](https://gitlab.com/monster-space-network/typemon/pipeline/commit/bb92377aff23f42f59555197ffbc4e0f58192766))
* resolver 추가 ([82c3ff7](https://gitlab.com/monster-space-network/typemon/pipeline/commit/82c3ff733031a070987748e3ca1ec13dcb7e7017))
* rxjs 의존성 삭제 ([942df4e](https://gitlab.com/monster-space-network/typemon/pipeline/commit/942df4e21231fdcd9284817d55d2470a10988400))
* rxjs 지원 추가 및 해결 방식 변경 ([f156731](https://gitlab.com/monster-space-network/typemon/pipeline/commit/f1567310f130638348ef0d225ca7456dc7128e86))
* **metadata-key:** 유형 변경 ([2c6ef3d](https://gitlab.com/monster-space-network/typemon/pipeline/commit/2c6ef3da90b695af79901c9b5072666da5e83361))
* **pipe:** 수명 옵션 추가 ([dea12a2](https://gitlab.com/monster-space-network/typemon/pipeline/commit/dea12a26769fd438d2d43db5a6f40e6ebfb09144))
* **pipe:** 옵션 이름 변경 ([a287221](https://gitlab.com/monster-space-network/typemon/pipeline/commit/a28722181d3f96b14a301a0ad6a9123f33fb330a))
* **pipe:** 인라이닝 기능 삭제 ([d5363a3](https://gitlab.com/monster-space-network/typemon/pipeline/commit/d5363a390446766d828079d826f2952315500ee4))
* rxjs 의존성 추가 ([c42230d](https://gitlab.com/monster-space-network/typemon/pipeline/commit/c42230db3f22745b31ccfeaf532eb95399f30289))



# [2.0.0-beta.2](https://gitlab.com/monster-space-network/typemon/pipeline/compare/2.0.0-beta.1...2.0.0-beta.2) (2020-01-26)


### Features

* **pipe:** 인라인 기능 추가 ([518693a](https://gitlab.com/monster-space-network/typemon/pipeline/commit/518693aaf8c9997d43286a732c358153c50ddcf9))



# [2.0.0-beta.1](https://gitlab.com/monster-space-network/typemon/pipeline/compare/2.0.0-beta.0...2.0.0-beta.1) (2020-01-26)


### Features

* 제공자 옵션 삭제, 의존성 주입 방식 변경 ([13c9385](https://gitlab.com/monster-space-network/typemon/pipeline/commit/13c9385741d391018eff0e4776e379bb5b792baf))



# [2.0.0-beta.0](https://gitlab.com/monster-space-network/typemon/pipeline/compare/1.1.0...2.0.0-beta.0) (2020-01-25)


### Features

* 의존성 변경 사항 반영 및 리팩터링 ([ae5d29f](https://gitlab.com/monster-space-network/typemon/pipeline/commit/ae5d29f7b677c2c4ae69de8ed15ccf205f5180f4))
* 의존성 업데이트 ([9c2f00f](https://gitlab.com/monster-space-network/typemon/pipeline/commit/9c2f00f76be1b748dbd32957f4cf5ac22e64ec7b))



# [1.1.0](https://gitlab.com/monster-space-network/typemon/pipeline/compare/1.0.0...1.1.0) (2019-12-12)


### Features

* pipeable 추가 ([35ec869](https://gitlab.com/monster-space-network/typemon/pipeline/commit/35ec869053a9ee32fc3d1094b7af9f166c35f608))



